import React from 'react';
import './App.css';
import './index.css';
import {
  Dropdown,
  Card,
  Row,
  Col,
} from 'react-bootstrap';
import Image from './Image';
import Image2 from './Image2';
import Image3 from './Image3';
import Title from './Title';
import Title2 from './Title2';
import Title3 from './Title3';
import Subtitle from './Subtitle';
import Subtitle2 from './Subtitle2';
import Subtitle3 from './Subtitle3';

function Header() {
  return (
    <div id="Header">
      <div className="Navbar">
        <div className="Nav-left">
          <a>
            <img 
              src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/typo_logo-7f995bc8faf05bc72aad13c551d939ef.svg"
              height="29.87px"
            />
          </a>
        </div>
        <div className="Nav-right">
          <Dropdown>
            <Dropdown.Toggle variant="danger" id="dropdown-basic">
              Bahasa
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item href="#/action-1">Bahasa Indonesia</Dropdown.Item>
              <Dropdown.Item href="#/action-2">English</Dropdown.Item>
              <Dropdown.Item href="#/action-3">日本語</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
            <a>
              Login
            </a>
            <button className="Btn">Daftar Gratis</button>
        </div>  
      </div>  
    </div>
    );
}

function Content() {
  return (
    <div id="Content-wrapper">
        <h2 className="Jumbotron">
          <span>Belajar Coding.
          <br/>
          Perluas Wawasan Anda.
          </span>
        </h2>
      <div className="Content-container">
        <div className="Landing-left">
          <img 
            src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/users-f8349e783e2091f6e9d6a84d6cb914d0.svg"
          />  
          <span className="Pengguna">1,900,000</span>
          <span>pengguna</span>
        </div>
        <div className="Landing-right">
          <img
            src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/countries-25dcf2bd3ea617a63080d7ac8e2baed4.svg"  
          />
          <span className="Spn1">aktif di</span>  
          <span className="Spn2">100+</span>  
          <span className="Spn3">negara</span>  
        </div>
      </div>
        <section>
            <button class="BrztNc">Mulai Sekarang</button>
        </section>    
    </div>
  );
}

function Program() {
  return (
  <>
    <div className="Animasi">

    </div>
    <div className="Program-container">
      <div className="Program-kerja">
        <h2 className="Jumbotron">
            Mulai perjalanan Anda dari pemula hingga menjadi seorang kreator
        </h2>
      </div>

      <div className="Image-container">
        <img 
          src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/slide_image_id-e013eefdcb66653da3668442c8985c9f.jpg"
          style={{margin: '72px 0px 0px',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  maxWidth: '1000px',
                  width: '100%'}}
        />
        <h3 style={{fontSize: '32px', margin: '40px 0px 0px', color: '#2b546a', textAlign: 'center', fontWeight: 'bold'}}>
          Slide Penuh Ilustrasi
        </h3>
        <p style={{margin: '16px 0px 0px', lineHeight: '2'}}>
          <span style={{color: '#8491A5'}}>
            Melalui representasi visual, setiap pelajaran di Progate berisi slide yang didesain secara khusus untuk
            <br/>
            meningkatkan pengalaman dan pemahaman belajar Anda.
          </span>
        </p>
      </div>

      <div className="Ilustrasi">
        <img
          src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/editor_image_id-ae285fbf5750c1ec91410115d1501895.jpg"
          style={{margin: '88px 0px 0px',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  maxWidth: '1000px',
                  width: '100%'}}
        />
        <h3 style={{fontSize: '32px', margin: '40px 0px 0px', color: '#2b546a', textAlign: 'center', fontWeight: 'bold'}}>
          Belajar dan Praktek Langsung
        </h3>
        <p style={{margin: '16px 0px 0px', lineHeight: '2'}}>
          <span style={{color: '#8491A5'}}>
            Semua yang Anda butuhkan untuk  belajar coding sudah disiapkan di dalam satu tampilan browser.            
            <br/>
            Di Progate,  Anda dapat langsung menerapkan apa yang baru Anda pelajari.
          </span>
        </p>
      </div>

      <div className="Ilustrasi">
        <img
          src="https://d2aj9sy12tbpym.cloudfront.net/assets/packs/dist/images/path_image_id-00de898f37d3fef9bc99ed6d8316dab8.png"
          style={{margin: '88px 0px 0px',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                  maxWidth: '1000px',
                  width: '100%'}}
        />
        <h3 style={{fontSize: '32px', margin: '40px 0px 0px', color: '#2b546a', textAlign: 'center', fontWeight: 'bold'}}>
          Pembelajaran Secara Terpandu
        </h3>
        <p style={{margin: '16px 0px 0px', lineHeight: '2'}}>
          <span style={{color: '#8491A5'}}>
            Ikuti serangkaian pelajaran yang disusun secara optimal untuk membantu Anda menjadi seorang kreator sejati.           
            <br/>
            Setelah selesai, Anda dapat menjadi coder dengan pengetahuan yang luas untuk mencapai cita-cita Anda secara mandiri.
          </span>
        </p>
      </div>
    </div>
  </>
  );
}

function Component(){
  return(
    <>
      <div className="Component-container">
        <h2 style={{textAlign: 'center', color: '#ffffff', margin: '0px'}}>Kisah Sukses</h2>
          <p style={{margin: '16px 0px 0px', color: '#ffffff', textAlign: 'center'}}>
            Bacalah kisah penuh inspirasi tentang kesuksesan para pelajar kami dari seluruh belahan dunia.
          </p>  
        <div className="Component-wrapper">  
            <div className="Card-component">
                <Image/>
                <Title/>
                <div className="Card-subtitle">
                  <Subtitle/>
                </div>  
            </div>

            <div className="Card-component">
                <Image2/>
                <Title2/>
              <div className="Card-subtitle">
                <Subtitle2/>
              </div>  
            </div>

            <div className="Card-component">
                <Image3/>
                <Title3/>
              <div className="Card-subtitle">
                <Subtitle3/>
              </div>  
            </div>
          </div>
          <div style={{textAlign: 'center', fontFamily: '"AvenirNext", "Lato", "Hirago KakuGothic ProN", Meiryo, sans-serif'}}>
            <button style={{width: '200px', border: 'none', lineHeight: '48px', padding: '0px 16px', borderRadius: '5px', fontSize: '14px', color: '#2B546A', fontWeight: 'bold'}}>
              Baca kisah lainnya
            </button>
          </div>  
        </div>
    </>  
  );
}

function Footer(){
  return (
    <div className="Footer-container">
      <div className="Footer-wrapper">
        <div className="Row">
        <div className="Satu">
          <img 
            src="https://d2aj9sy12tbpym.cloudfront.net/assets/landing/primary_logo-4d1810538e410b4c6af84210420099ca1772e8cb39013fad8532e499bcdb136e.svg"
            style={{width: '144px', margin: '4px 0px 0p'}}
          />
        </div>
        <div className="Dua">
          <h4>LAYANAN</h4>
            <ul>
              <li><a href="#">Pelajaran</a></li>
              <li><a href="#">Paket</a></li>
              <li><a href="#">Kisah Sukses</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Bantuan</a></li>
            </ul>
        </div>
        <div className="Tiga">
          <h4>DUKUNGAN</h4>
            <ul>
              <li><a href="#">Tentang Progate</a></li>
              <li><a href="#">Ketentuan</a></li>
              <li><a href="#">Kebijakan Privasi</a></li>
            </ul>
        </div>
        <div className="Empat">
          <h4>IKUTI KAMI</h4>
            <ul>
              <li><a href="#">Instagram</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="https://www.facebook.com">Facebook</a></li>
            </ul>
        </div>
        </div>
      </div>
    </div>
  )
}

function App() {
  return (
      <> 
        <Header/>
        <Content/>
        <Program/>
        <Component/>
        <Footer/>
      </>  
  );
}

export default App;
