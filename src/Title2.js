import React from 'react';

class Title2 extends React.Component {
	constructor(){
	super();
		this.state = {
			title: "Bagaimana Seorang Programmer Otodidak Sekaligus Progate Student Ambassador Diterima Bekerja sebagai Analis Keuangan di 'Barclays'!"
		}
	}

	render(){
		return(
			<div style={{color: '#2B546A', fontFamily: 'AvenirNext" "Lato" "Hirago KakuGothic ProN" Meiryo sans-serif', margin: '40px 0px 0px'}}>
				{this.state.title};
			</div>
		)
	}
}

export default Title2;