import React from 'react';

class Image extends React.Component{
	constructor(){
		super();
		this.state = {
			image: "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/success_story/ymihashi-id/profile_image/1584593813722.jpg",
		}
	}

	render(){
		return(
			<div>
				<img 
					src={this.state.image}
					style={{width: '100px', height: '100px', borderRadius: '100%'}}
				/>
			</div>
			)
	}
}

export default Image;