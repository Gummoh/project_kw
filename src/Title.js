import React from 'react';

class Title extends React.Component {
	constructor(){
	super();
		this.state = {
			title: "Perjalanan Anak Usia 15 Tahun dari Programming Scratch Ke “Super Creator”"
		}
	}

	render(){
		return(
			<div>
			   	<p style={{color: '#2B546A', fontFamily: 'AvenirNext" "Lato" "Hirago KakuGothic ProN" Meiryo sans-serif', margin: '40px 0px 0px'}}>
				  {this.state.title};
				</p>
			</div>
		)
	}
}

export default Title;