import React from 'react';

class Title3 extends React.Component {
	constructor(){
	super();
		this.state = {
			title: "Bagaimana Seorang Siswa Bisnis di Rwanda Menjadi Developer di Andela dalam waktu 6 Bulan?"
		}
	}

	render(){
		return(
			<div style={{color: '#2B546A', fontFamily: 'AvenirNext" "Lato" "Hirago KakuGothic ProN" Meiryo sans-serif', margin: '40px 0px 0px'}}>
				{this.state.title};
			</div>
		)
	}
}

export default Title3;