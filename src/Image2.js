import React from 'react';

class Image2 extends React.Component{
	constructor(){
		super();
		this.state = {
			image: "https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/success_story/rhythmv-id/profile_image/1584593400936.jpg"
		}
	}

	render(){
		return(
			<div>
				<img 
					src={this.state.image}
					style={{width: '100px', height: '100px', borderRadius: '100%'}}
				/>
			</div>
			)
	}
}

export default Image2;