import React from 'react';

class Subtitle2 extends React.Component {
	constructor(){
		super();
		this.state = {
			name: "Rhythm Varshney",
			negara: "India"
		}
	}

	render(){
		return(
			<div style={{display: 'flex', flexDirection: 'column', justifyContent: 'flex-end'}}>
				<p style={{color: '#BAC6D3', margin:'32px 0px 0px'}}>{this.state.name}</p>
				<p style={{color: '#BAC6D3', margin: '2px 0px 0px', fontSize: '12px'}}>{this.state.negara}</p>
			</div>
		)
	}
}

export default Subtitle2;